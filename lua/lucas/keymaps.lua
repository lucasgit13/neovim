local opts = { noremap = true, silent = true }
local keymap= vim.keymap.set

keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

------------
-- NORMAL --
------------
keymap("n", "<leader>wc", ":wincmd c<cr>", opts)
keymap("n", "<leader>wo", ":wincmd o<cr>", opts)
keymap("n", "<leader>wk", ":bw!<cr>", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

keymap("n", "Y", "yg_", opts)
keymap("n", "<leader>v", "vg_", opts)

-- Yank to system clipboard
keymap({"n", "v"}, "<C-y>", "\"+y", opts)

-- Paste from system clipboard
keymap({"n", "v"}, "<C-p>", "\"+p==", opts)

-- Move text up and down
keymap("n", "<A-j>", ":m .1<CR>==", opts)
keymap("n", "<A-k>", ":m .-2<CR>==", opts)

keymap("n", "<leader><cr>", ":so % | echo expand('%:t') 'was sourced!'<cr>", {})
keymap("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

keymap("n", "<Esc>", ":noh<cr>", opts)
keymap("n", "<leader>ps", "<cmd>Lazy sync<cr>", opts)
-- keymap("n", "<leader>ps", ":PackerSync<cr>", opts)

------------
-- VISUAL --
------------
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m '>+1<CR>gv=gv", opts)
keymap("v", "<A-k>", ":m '<-2<CR>gv=gv", opts)

keymap("v", "p", '"_dP', opts)
keymap("v", "<leader>d", '"_d', opts)

---------------------
-- VISUAL & INSERT --
---------------------
keymap({"v", "i" }, "jk", "<ESC>", opts)


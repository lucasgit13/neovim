local options = {
    colorcolumn = '80',
    clipboard = "unnamedplus",
    backup = false,
    cmdheight = 2,
    completeopt = { "menuone", "noselect" },
    cursorline = true,
    expandtab = true,
    fileencoding = "utf-8",
    guicursor = '',
    hidden = true,
    laststatus = 3,
    mouse = "a",
    number = true,
    pumheight = 10,
    relativenumber = true,
    scrolloff = 8,
    shiftwidth = 4,
    showcmd = true,
    showmode = true,
    sidescrolloff = 8,
    signcolumn = "yes",
    smartcase = true,
    smartindent = true,
    splitbelow = true,
    splitright = true,
    swapfile = false,
    tabstop = 4,
    termguicolors = true,
    timeoutlen = 300,
    undofile = true,
    updatetime = 300,
    wrap = false,
    writebackup = false,
}

vim.cmd [[
    " set winbar=%=%m\ %f
    hi Normal guibg=NONE ctermbg=NONE
    highlight WinSeparator guibg=None
    hi ColorColumn ctermbg=lightgray guibg=gray
    colorscheme gruvbox-material
]]


for k, v in pairs(options) do
    vim.opt[k] = v
end


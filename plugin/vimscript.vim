autocmd BufReadPost * silent! normal! g`"zv
autocmd BufRead,BufEnter,BufNewFile *.s setlocal commentstring=#\ %s
autocmd BufRead sxhkdrc setlocal commentstring=#\ %s
autocmd! bufwritepost sxhkdrc :!pkill -USR1 -x sxhkd

function StripTrailingWhitespace()
    if !&binary && &filetype != 'diff'
        normal mz
        normal Hmy
        %s/\s\+$//e
        normal 'yz<CR>
        normal `z
        endif
        endfunction

autocmd! bufwritepost  * call StripTrailingWhitespace()

nnoremap <ESC> :noh<CR>

" let g:UltiSnipsExpandTrigger="<tab>"
" let g:UltiSnipsJumpForwardTrigger="<c-j>"
" let g:UltiSnipsJumpBackwardTrigger="<c-k>"
" let g:UltiSnipsSnippetDirectories="~/.config/nvim/snippets/"
